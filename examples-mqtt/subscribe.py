import time
import network
from machine import Pin
from umqtt.robust import MQTTClient

led = Pin(5,Pin.OUT)
led.value(1)  # turn it off

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
print("*** Connecting to WiFi...")
wlan.connect("SSID","PASS")
while not wlan.isconnected():
    time.sleep(0.5)
print("*** Wifi connected")

def sub_callback(topic,payload):
    if topic == b"ku/cpe/NICKNAME/led":
        try:
            led.value(1-int(payload))
        except ValueError:
            pass

mqtt = MQTTClient("UNIQUE-ID","BROKER")
print("*** Connecting to MQTT broker...")
mqtt.connect()
print("*** MQTT broker connected")
mqtt.set_callback(sub_callback)
mqtt.subscribe(b"ku/cpe/NICKNAME/led")

while True:
    mqtt.check_msg()

