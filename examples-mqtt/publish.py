import time
import network
from machine import Pin, ADC
from umqtt.robust import MQTTClient

ldr = ADC(Pin(34))
ldr.atten(ADC.ATTN_11DB)

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
print("*** Connecting to WiFi...")
wlan.connect("SSID","PASS")
while not wlan.isconnected():
    time.sleep(0.5)
print("*** Wifi connected")

mqtt = MQTTClient("UNIQUE-ID","BROKER")
print("*** Connecting to MQTT broker...")
mqtt.connect()
print("*** MQTT broker connected")

while True:
    value = ldr.read()
    print("Publish light:", value)
    mqtt.publish("ku/cpe/NICKNAME/light", str(value))
    time.sleep(2)

